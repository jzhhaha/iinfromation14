import logging
from flask import session
from flask_script import Manager
from flask_migrate import  Migrate,MigrateCommand
from info import create_app,db,models
#manage是程序的启动入口，不关心相关业务逻辑
from info.models import User

app = create_app('development')
manager=Manager(app)
Migrate(app,db)
manager.add_command('db',MigrateCommand)


@manager.option('-p,''-password',dest='password')
@manager.option('-n,''-name',dest='name')
def createsuperuser(name,password):
    if not all([name,password]):
        print('参数不足')
        return
    user=User()
    user.mobile=name
    user.nick_name=name
    user.password=password
    user.is_admin=True
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        print(e)
        db.session.rollback()


if __name__ == '__main__':
    print(app.url_map)
    manager.run()