function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

$(function () {

    $(".base_info").submit(function (e) {
        e.preventDefault()

        var signature = $("#signature").val()
        var nick_name = $("#nick_name").val()
        var gender = $("input[name='gender']:checked").val()

        if (!nick_name) {
            alert('请输入昵称')
            return
        }
        if (!gender) {
            alert('请选择性别')
        }
        var params={
            "nick_name":nick_name,
            "signature":signature,
            "gender":gender
        }
        $.ajax({
            url:"/profile/user_base",
            type:"post",
            contentType:"application/json",
            data:JSON.stringify(params),
            headers:	{
                "X-CSRFToken":	getCookie('csrf_token')
            },
            success:function (resp) {
                if(resp.errno=="0"){
                    alert(resp.data.user.nick_name)
                    $('.user_center_name', parent.document).html(params['nick_name'])
                    $('#nick_name', parent.document).html(params['nick_name'])
                    $('.input_sub').blur()
                }else {
                    alert(resp.errmsg)
                }
            }

        })
    })
})